if [ -z "${__LIBZEPHIR_COMPOSE_LOADED__}" ]; then
    __LIBZEPHIR_COMPOSE_LOADED__=1 # Avoid multiple loading


function die { echo $@ >&2; exit 1; }

function help {
    cat <<EOF
Utilitaire pour le développement de Zéphir.
Permet de gérer un environnement Zéphir personnalisé.

Usage: $0 [options...] -- <arguments docker-compose>

Options:

  -h|--help                   Afficher l'aide

  -b|--base     <ENV_NAME>    Environnement de base de Zéphir à utiliser. La liste
                              des environnements est :
                              - "stable": utilise les dernières images docker stables
                              - "staging": utilise les dernières images docker d’intégration
                              - "dev": utilise les dernières images docker de développement
                              - "src": utilise les dépôts de source Git clonés dans "${REPOS_DIR}"
                              Par défaut: "$DEFAULT_ZEPHIR_ENV".
  --without     <SERVICE>     Exclure le service spécifié
  --stable      <SERVICE>     Utiliser le service spécifié depuis la dernière image docker stable
  --staging     <SERVICE>     Utiliser le service spécifié depuis la dernière image docker de démonstration
  --dev         <SERVICE>     Utiliser le service spécifié depuis la dernière image docker de développement
  --src         <SERVICE>     Utiliser le service spécifié depuis le code source
                              l’argument <SERVICE> définie soit :
                              - le nom du service dont le dépôt de source Git se situe dans "${REPOS_DIR}"
                              - le chemin, relatif ou absolue, où se situe le dépôt Git des sources du service
  --helpers                   Utiliser également les services "helpers"
  --tests                     Utiliser également les services "tests"
  --clone                     Cloner automatiquement les dépôts Git lorsque cela est nécessaire

Arguments docker-compose:
  up                          Démarrer les conteneurs depuis les fichiers docker-compose
    option: -b                Libérer la console à la fin de l'exécution
  down                        Arrêter les conteneurs ayant été généré par les fichiers docker-compose
    option: -v                Supprimer les volumes au moment de l'arrêt des conteneurs
  config                      Afficher le fichier docker-compose compilé

Si le fichier "${APP_DIR}/${OPTIONS_FILE}" existe le script l'utilisera également comme source d'options.
EOF
}

function check_defined {
    local variable=$1
    if [ -z "${!variable}" ]
    then
        die "Required variable '${variable}' is undefined"
    fi
}

function parse_options {

    local options
    if ! options=$(getopt -u -o hb: -l help,base:,src:,dev:,staging:,stable:,without:,helpers,clone,tests -- "$@")
    then
        help
        exit 1
    fi

    set -- $options

    while [ $# -gt 0 ]
    do
        case $1 in
            -h|--help)
                help
                exit 0
                ;;
            -b|--base)
                ZEPHIR_ENV="$2"
                shift;
                ;;
            --src)
                SRC_SERVICES+=("$2")
                shift;
                ;;
            --dev)
                DEV_SERVICES+=("$2")
                shift;
                ;;
            --staging)
                STAGING_SERVICES+=("$2")
                shift;
                ;;
            --stable)
                STABLE_SERVICES+=("$2")
                shift;
                ;;
            --without)
                WITHOUT_SERVICES+=("$2")
                shift;
                ;;
            --helpers)
                WITH_HELPERS=yes
                shift;
                ;;
            --tests)
                WITH_TESTS=yes
                shift;
                ;;
            --clone)
                AUTO_CLONE=yes
                ;;
            (--) shift; break;;
            (*) break;;
        esac
        shift
    done

    # Save remaining arguments as docker-compose's ones
    DC_ARGS+=($@)
}

## Strip any path component and YAML extension
function get_service_name {
    basename "${1}" .yml
}

function get_variable_prefix {
    local name="${1//-/_}"
    echo "${name^^}"
}

function get_service_source_path {
    local name=$(get_service_name "${1}")
    local raise_without_source="${2}"

    if readlink -e "${1}" > /dev/null
    then
        path=$(readlink -e "${1}")
    elif readlink -e "${REPOS_DIR}/${name}" > /dev/null
    then
        path=$(readlink -e "${REPOS_DIR}/${name}")
    elif [ "${AUTO_CLONE}" = 'yes' ]
    then
        echo -n "Cloning the Git source repository for service '${name}' as '${REPOS_DIR}/${name}'... " >&2
        if git clone -b develop "${DEFAULT_GIT_REPO_BASE_URL}/${name}" "${REPOS_DIR}/${name}" 2> /dev/null
        then
	    echo "OK." >&2
            path=$(readlink -e "${REPOS_DIR}/${name}")
        else
            if [ "${raise_without_source}" = 'no' ]
            then
                echo "skipping missing sources." >&2
            else
                die "unable to clone the Git repository."
            fi
        fi
    elif [ "${raise_without_source}" = 'no' ]
    then
        echo "Warning: no source directory for service '${name}'." >&2
    else
        die "Unable to find directory for service '${name}'."
    fi
    echo "${path}"
}

function get_service_compose_file {
    local zephir_env=$1
    local service_name=$2
    echo "$(get_zephir_env_dir $zephir_env)/${service_name}.yml"
}

function get_zephir_env_dir {
    local zephir_env="$1"
    case $zephir_env in
        src)
            echo "$(realpath -e ${SELF_DIR}/src)"
            return 0
            ;;
        tagged|dev|staging|stable)
            echo "$(realpath -e ${SELF_DIR}/tagged)"
            return 0
            ;;
        helpers)
            echo "$(realpath -e ${SELF_DIR}/helpers)"
            return 0
            ;;
        test)
            echo "$(realpath -e ${SELF_DIR}/test)"
            return 0
            ;;
        *)
            echo 1>&2 "Environnement non géré: '${zephir_env}'"
            help
            exit 1
            ;;
    esac
}

function get_env_compose_files {
    local zephir_env="$1"
    local without=${@:2}
    local zephir_env_dir=$(get_zephir_env_dir "$zephir_env")
    local files=($(find "$zephir_env_dir" -name "*.yml"))
    local filtered_files=()

    for f in ${files[@]}; do
        for ff in ${without[@]}; do
            if [ "$(basename $ff)" == "$(basename $f)" ]; then
                continue 2
            fi
        done
        filtered_files+=($f)
    done

    echo ${filtered_files[@]}
}

fi #ifdef __LIBZEPHIR_COMPOSE_LOADED__
